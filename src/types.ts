export type StepParams = Record<string, string>;

export type Step = {
  id: string;
  type: "read" | "write";
  app: string;
  action: string;
  title: string;
  params: StepParams;
};

export type StepProperty = "id" | "type" | "title" | "app" | "action";

export type Zap = {
  id: string;
  title: string;
  steps: Step[];
};
