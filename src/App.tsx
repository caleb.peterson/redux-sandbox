import React from "react";
import { ZapEditor } from "./components/ZapEditor";
import { ReselectZap } from "./components/ReselectZap";
import { SelectorKitZap } from "./components/SelectorKitZap";

function App() {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        gap: 20,
        height: "100%",
        alignItems: "stretch",
      }}
    >
      <ZapEditor />
      <ReselectZap />
      <SelectorKitZap />
    </div>
  );
}

export default App;
