import { configureStore } from "@reduxjs/toolkit";
import { counterReducer } from "./state/counter";
import { zapReducer } from "./state/zap";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    zap: zapReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
