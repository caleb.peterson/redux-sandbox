import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { Zap } from "../types";
import { uuid4 } from "../utils/uuid4";

export interface ZapState {
  value: Zap;
}

const initialState: ZapState = {
  value: {
    id: uuid4(),
    title: "My Zap",
    steps: [
      {
        id: uuid4(),
        type: "read",
        app: "CodeAPI",
        action: "run_javascript",
        title: "My Trigger",
        params: { code: "1 + 1" },
      },
      {
        id: uuid4(),
        type: "write",
        app: "CodeAPI",
        action: "run_javascript",
        title: "Action 1",
        params: { code: "2 + 2" },
      },
      {
        id: uuid4(),
        type: "write",
        app: "CodeAPI",
        action: "run_javascript",
        title: "Action 2",
        params: { code: "3 + 3" },
      },
    ],
  },
};

export const zapSlice = createSlice({
  name: "zap",
  initialState,
  reducers: {
    update: (state, action: PayloadAction<Zap>) => {
      state.value = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { update } = zapSlice.actions;

export const zapReducer = zapSlice.reducer;
