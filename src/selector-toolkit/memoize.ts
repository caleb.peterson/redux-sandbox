type Fn<TArg, TResult> = (arg: TArg) => TResult;

type Primitive = string | number | boolean;

// Source: https://github.com/timkendrick/memoize-weak/blob/master/lib/memoize.js
const isPrimitive = (value: unknown): value is Primitive =>
  (typeof value !== "object" && typeof value !== "function") || value === null;

// Source: https://github.com/emotion-js/emotion/blob/main/packages/weak-memoize/src/index.js
export const memoize = <TArg, TResult>(
  fn: Fn<TArg, TResult>
): Fn<TArg, TResult> => {
  const cache: WeakMap<object, TResult> = new WeakMap();
  const primitiveKeys = new Map<Primitive, object>();

  const resolveKey = (arg: TArg): object => {
    if (isPrimitive(arg)) {
      const primitive = primitiveKeys.get(arg) ?? {};
      if (!primitiveKeys.has(arg)) {
        primitiveKeys.set(arg, primitive);
      }

      return primitive;
    }

    return arg as object;
  };

  return (arg: TArg): TResult => {
    const key = resolveKey(arg);

    if (cache.has(key)) {
      return cache.get(key) as TResult;
    }

    const ret = fn(arg);

    cache.set(key, ret);

    return ret;
  };
};
