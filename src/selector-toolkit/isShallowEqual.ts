export const isShallowEqual = (a: unknown, b: unknown) => a === b;
