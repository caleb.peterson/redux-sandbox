import { memoize } from "./memoize";
import { Selector, Accessors } from "./types";

export const getAccessors = memoize(<TState>(state: TState) => {
  const ifc: Accessors<TState> = {
    get: <TResult>(sel: Selector<TState, TResult>) => {
      return sel(state);
    },

    get state() {
      return state;
    },
  };

  return ifc;
});
