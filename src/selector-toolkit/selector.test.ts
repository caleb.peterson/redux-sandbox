import { useSelector } from "react-redux";
import { createSelectorToolkit } from "./createSelectorToolkit";

// State fixture
const state = {
  firstName: "John",
  lastName: "Doe",
};

// State type
type RootState = typeof state;

// Create the selector kit instance
const { selector } = createSelectorToolkit<RootState>(useSelector);

const firstNameState = selector<string>({
  get: ({ state }) => state.firstName,
});

const lastNameState = selector<string>({
  get: ({ state }) => state.lastName,
});

const fullNameState = selector<string>({
  get: ({ get }) => get(firstNameState) + " " + get(lastNameState),
});

describe("selector", () => {
  it("should select a value directly from the state", () => {
    expect(firstNameState(state)).toEqual("John");
  });

  it("should select a derived value composed from other selectors", () => {
    expect(fullNameState(state)).toEqual("John Doe");
  });
});
