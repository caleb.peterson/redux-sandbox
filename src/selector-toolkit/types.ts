type Primitive = undefined | null | boolean | number | symbol | string;
interface HasToJSON {
  toJSON(): SerializableParam;
}

export type SerializableParam =
  | Primitive
  | HasToJSON
  | ReadonlyArray<SerializableParam>
  | ReadonlySet<SerializableParam>
  | ReadonlyMap<SerializableParam, SerializableParam>
  | Readonly<{ [key: string]: SerializableParam }>;

export type EqualityFn<T> = (a: T, b: T) => boolean;

export interface Accessors<TState> {
  get: <TResult>(selector: Selector<TState, TResult>) => TResult;
  state: TState;
}

type BaseSelector<TResult> = {
  // FIXME: this is currently un-used, but it should be to provide a valuable optimization escape hatch
  equality?: EqualityFn<TResult>;
};

export interface SelectorSpec<TState, TResult> {
  get: (ifc: Accessors<TState>) => TResult;
  equality?: EqualityFn<TResult>;
}

type SelectorFactory<TState, TResult> = (
  spec: SelectorSpec<TState, TResult>
) => (state: TState) => TResult;

export type Selector<TState, TResult> = BaseSelector<TResult> &
  ReturnType<SelectorFactory<TState, TResult>>;

export interface SelectorFamilySpec<TState, Key, TResult> {
  get: (key: Key) => (ifc: Accessors<TState>) => TResult;
  equality?: EqualityFn<TResult>;
}

type SelectorFamilyFactory<TState, Key extends SerializableParam, TResult> = (
  spec: SelectorFamilySpec<TState, Key, TResult>
) => (key: Key) => Selector<TState, TResult>;

export type SelectorFamily<
  TState,
  Key extends SerializableParam,
  TResult
> = ReturnType<SelectorFamilyFactory<TState, Key, TResult>>;
