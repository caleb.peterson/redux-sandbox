import { useSelector } from "react-redux";
import { createSelectorToolkit } from "./createSelectorToolkit";

// State fixture
const state = {
  firstName: "John",
  lastName: "Doe",
};

// State type
type RootState = typeof state;

// Create the selector kit instance
const { selector, selectorFamily } =
  createSelectorToolkit<RootState>(useSelector);

const propertyState = selectorFamily<string, keyof RootState>({
  get:
    (property) =>
    ({ state }) =>
      state[property],
});

const fullNameState = selector<string>({
  get: ({ get }) =>
    get(propertyState("firstName")) + " " + get(propertyState("lastName")),
});

describe("selectorFamily", () => {
  it("should select a derived value composed from other selectors", () => {
    expect(fullNameState(state)).toEqual("John Doe");
  });
});
