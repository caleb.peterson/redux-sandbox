import { useSelector } from "react-redux";

import { Selector } from "./types";

/**
 * NOT A PART OF THE IMPLEMENTATION
 *
 * This hook is not a part of the `selector-toolkit` implementation.
 *
 * It exists solely as a point of reference for the useTrackingSelector
 * to be compared against for memoization/performance.
 */
export const useValue = <TState = unknown, TResult = unknown>(
  sel: Selector<TState, TResult>
) => useSelector(sel, sel.equality);
