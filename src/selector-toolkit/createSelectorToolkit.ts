import { createTrackedSelector } from "react-tracked";

import { memoize } from "./memoize";
import { getAccessors } from "./getAccessors";
import { isShallowEqual } from "./isShallowEqual";
import {
  Selector,
  SelectorSpec,
  SelectorFamily,
  SelectorFamilySpec,
  SerializableParam,
} from "./types";

type UseSelector<TState> = <TResult>(
  selector: (state: TState) => TResult
) => TResult;

type SelectorConstruction<TState, TResult> = Omit<
  Selector<TState, TResult>,
  "equality"
> &
  Partial<Selector<TState, TResult>>;

/**
 * The sole purpose of this factory is to close over the `TState` type so that it
 * doesn't have to be specified in every `selector`/`selectorFamily` declaration.
 */
export const createSelectorToolkit = <TState>(
  useSelector: UseSelector<TState>
) => {
  // Create the selector factory
  const selector = <TResult = unknown>(
    spec: SelectorSpec<TState, TResult>
  ): Selector<TState, TResult> => {
    const getter = spec.get;

    const constructed: SelectorConstruction<TState, TResult> = memoize(
      (state: TState) => getter(getAccessors(state))
    );

    constructed.equality = spec.equality ?? isShallowEqual;

    return constructed as Selector<TState, TResult>;
  };

  // Create the selectorFamily factory
  const selectorFamily = <TResult, Key extends SerializableParam>(
    spec: SelectorFamilySpec<TState, Key, TResult>
  ): SelectorFamily<TState, Key, TResult> => {
    const familyGetter = memoize(spec.get);

    const selectorFactory = memoize((key: Key) => {
      const constructed: SelectorConstruction<TState, TResult> = memoize(
        (state: TState) => {
          const getter = familyGetter(key);
          return getter(getAccessors(state));
        }
      );

      constructed.equality = spec.equality ?? isShallowEqual;

      return constructed as Selector<TState, TResult>;
    });

    return selectorFactory;
  };

  // Create the useSelectorValue hook
  const useTrackedState = createTrackedSelector<TState>(useSelector);
  const useSelectorValue = <TResult>(sel: Selector<TState, TResult>) => {
    const result = sel(useTrackedState());
    return result;
  };

  // Return the Selector Toolkit
  return {
    selector,
    selectorFamily,
    useSelectorValue,
  };
};

export type SelectorToolkit<TState> = ReturnType<
  typeof createSelectorToolkit<TState>
>;
