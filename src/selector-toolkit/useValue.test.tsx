import { act, render, renderHook, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { PayloadAction, configureStore, createSlice } from "@reduxjs/toolkit";

import { useValue } from "./useValue";
import { createSelectorToolkit } from "./createSelectorToolkit";
import { FC, PropsWithChildren } from "react";
import { Provider, useDispatch, useSelector } from "react-redux";
import { Accessors, Selector } from "./types";

// State fixture
const initialState = {
  firstName: "John",
  lastName: "Doe",
};

type PersonState = typeof initialState;

const personSlice = createSlice({
  name: "person",
  initialState,
  reducers: {
    changeFirstName: (state, action: PayloadAction<string>) => {
      state.firstName = action.payload;
    },
    changeLastName: (state, action: PayloadAction<string>) => {
      state.lastName = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
const { changeFirstName, changeLastName } = personSlice.actions;

const personReducer = personSlice.reducer;

const setupTestStore = () =>
  configureStore({
    reducer: {
      person: personReducer,
    },
  });

let store = setupTestStore();

// Infer the `RootState` and `AppDispatch` types from the store itself
type RootState = ReturnType<typeof store.getState>;

const wrapper: FC<PropsWithChildren> = ({ children }) => (
  <Provider store={store}>{children}</Provider>
);

// Create the selector kit instance
const { selector, selectorFamily } =
  createSelectorToolkit<RootState>(useSelector);

const personPropertyState = selectorFamily<string, keyof PersonState>({
  get:
    (property) =>
    ({ state }) =>
      state.person[property],
});

const fullNameState = selector({
  get: ({ get }) =>
    get(personPropertyState("firstName")) +
    " " +
    get(personPropertyState("lastName")),
});

const useTestHarness = <TResult,>(selector: Selector<RootState, TResult>) => {
  const dispatch = useDispatch();
  const value = useValue(selector);

  return {
    value,
    dispatch,
  };
};

describe("useValue", () => {
  beforeEach(() => {
    store = setupTestStore();
  });

  describe("behavioral expectations", () => {
    it("correctly selects a state value before and after it is changed", () => {
      const { result } = renderHook(
        () => useTestHarness(personPropertyState("firstName")),
        {
          wrapper,
        }
      );

      expect(result.current.value).toEqual("John");

      act(() => {
        result.current.dispatch(changeFirstName("William"));
      });

      expect(result.current.value).toEqual("William");
    });

    it("correctly selects a derived value before and after it is changed", () => {
      const { result } = renderHook(() => useTestHarness(fullNameState), {
        wrapper,
      });

      expect(result.current.value).toEqual("John Doe");

      act(() => {
        result.current.dispatch(changeFirstName("William"));
      });

      expect(result.current.value).toEqual("William Doe");
    });
  });

  describe("performance expectations", () => {
    it("does not trigger a needless re-render when the selected value does not change", () => {
      const getFirstName = jest.fn(
        ({ state }: Accessors<RootState>) => state.person.firstName
      );
      const firstNameState = selector({
        get: getFirstName,
      });

      // Wrapped in jest.fn so that renders can be counted
      const TestHarness: FC = jest.fn(() => {
        const { value, dispatch } = useTestHarness(firstNameState);

        const onChangeFirstName = () => {
          dispatch(changeFirstName("William"));
        };

        const onChangeLastName = () => {
          dispatch(changeLastName("Scott"));
        };

        return (
          <>
            <div data-testid="value">{value}</div>
            <button data-testid="change-first-name" onClick={onChangeFirstName}>
              Change First Name
            </button>
            <button data-testid="change-last-name" onClick={onChangeLastName}>
              Change Last Name
            </button>
          </>
        );
      });

      render(<TestHarness />, { wrapper });

      // The initial render displays the correct value
      expect(screen.getByTestId("value")).toHaveTextContent("John");
      // The initial render had to run the firstNameState selector
      expect(getFirstName).toHaveBeenCalledTimes(1);
      // The initial render counts as 1 render
      expect(TestHarness).toHaveBeenCalledTimes(1);

      // Change the last name
      userEvent.click(screen.getByTestId("change-last-name"));

      // The first name is still correct
      expect(screen.getByTestId("value")).toHaveTextContent("John");
      // The re-render had to run the firstNameState selector again to determine
      // that the selected value did not change even though the root state did
      expect(getFirstName).toHaveBeenCalledTimes(2);
      // There have been no additional renders because no relevant values have changed
      expect(TestHarness).toHaveBeenCalledTimes(1);

      // Now change the first name
      userEvent.click(screen.getByTestId("change-first-name"));

      // The first name has been updated
      expect(screen.getByTestId("value")).toHaveTextContent("William");
      // The re-render had to run the firstNameState selector again
      expect(getFirstName).toHaveBeenCalledTimes(3);
      // An additional render occurred because a relevant value changed
      expect(TestHarness).toHaveBeenCalledTimes(2);
    });
  });
});
