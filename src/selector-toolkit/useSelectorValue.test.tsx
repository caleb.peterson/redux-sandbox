import { act, render, renderHook, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { PayloadAction, configureStore, createSlice } from "@reduxjs/toolkit";

import { createSelectorToolkit } from "./createSelectorToolkit";
import { FC, PropsWithChildren, Suspense, useState } from "react";
import { Provider, useDispatch, useSelector } from "react-redux";
import { Accessors, Selector } from "./types";
import { memoize } from "lodash";

// State fixture
const initialState = {
  firstName: "John",
  lastName: "Doe",
};

type PersonState = typeof initialState;

const personSlice = createSlice({
  name: "person",
  initialState,
  reducers: {
    changeFirstName: (state, action: PayloadAction<string>) => {
      state.firstName = action.payload;
    },
    changeLastName: (state, action: PayloadAction<string>) => {
      state.lastName = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
const { changeFirstName, changeLastName } = personSlice.actions;

const personReducer = personSlice.reducer;

const setupTestStore = () =>
  configureStore({
    reducer: {
      person: personReducer,
    },
  });

let store = setupTestStore();

// Infer the `RootState` and `AppDispatch` types from the store itself
type RootState = ReturnType<typeof store.getState>;

const Suspended = () => <div data-testid="loading" />;

const wrapper: FC<PropsWithChildren> = ({ children }) => (
  <Provider store={store}>
    <Suspense fallback={<Suspended />}>{children}</Suspense>
  </Provider>
);

// Create the selector kit instance
const { selector, selectorFamily, useSelectorValue } =
  createSelectorToolkit<RootState>(useSelector);

const personPropertyState = selectorFamily<string, keyof PersonState>({
  get:
    (property) =>
    ({ state }) =>
      state.person[property],
});

const fullNameState = selector({
  get: ({ get }) =>
    get(personPropertyState("firstName")) +
    " " +
    get(personPropertyState("lastName")),
});

const useTestHarness = <TResult,>(selector: Selector<RootState, TResult>) => {
  const dispatch = useDispatch();
  const value = useSelectorValue(selector);

  return {
    value,
    dispatch,
  };
};

describe("useSelectorValue", () => {
  beforeEach(() => {
    store = setupTestStore();
  });

  describe("behavioral expectations", () => {
    it("correctly selects a state value before and after it is changed", () => {
      const { result } = renderHook(
        () => useTestHarness(personPropertyState("firstName")),
        {
          wrapper,
        }
      );

      expect(result.current.value).toEqual("John");

      act(() => {
        result.current.dispatch(changeFirstName("William"));
      });

      expect(result.current.value).toEqual("William");
    });

    it("correctly selects a derived value before and after it is changed", () => {
      const { result } = renderHook(() => useTestHarness(fullNameState), {
        wrapper,
      });

      expect(result.current.value).toEqual("John Doe");

      act(() => {
        result.current.dispatch(changeFirstName("William"));
      });

      expect(result.current.value).toEqual("William Doe");
    });

    it("caches selectorFamily results based on key and only re-runs for a different key when no state is consumed", () => {
      // FIXME: document this expectation as a recipe instead - memoize the calculation function itself instead of the selector
      const count = jest.fn();
      const calculateValue = memoize((query: string) => {
        count();
        return `calculated value: ${query}`;
      });

      const calculatedState = selectorFamily<string, string>({
        get: (query) => () => {
          return calculateValue(query);
        },
      });

      const { result, rerender } = renderHook(
        (query: string) => useTestHarness(calculatedState(query)),
        {
          wrapper,
          initialProps: "A",
        }
      );

      expect(result.current.value).toEqual("calculated value: A");
      expect(count).toHaveBeenCalledTimes(1);

      // Re-render with the same key does not re-run the selector
      rerender("A");

      expect(result.current.value).toEqual("calculated value: A");
      expect(count).toHaveBeenCalledTimes(1);

      // Un-related state change does not re-run the selector
      act(() => {
        result.current.dispatch(changeFirstName("William"));
      });

      expect(result.current.value).toEqual("calculated value: A");
      expect(count).toHaveBeenCalledTimes(1);

      // Re-render with a different key does re-run the selector
      rerender("B");

      expect(result.current.value).toEqual("calculated value: B");
      expect(count).toHaveBeenCalledTimes(2);
    });
  });

  describe("performance expectations", () => {
    it("does not trigger a needless re-render when the selected value does not change", () => {
      const getFirstName = jest.fn(
        ({ state }: Accessors<RootState>) => state.person.firstName
      );
      const firstNameState = selector({
        get: getFirstName,
      });

      // Wrapped in jest.fn so that renders can be counted
      const TestHarness: FC = jest.fn(() => {
        const { value, dispatch } = useTestHarness(firstNameState);

        const onChangeFirstName = () => {
          dispatch(changeFirstName("William"));
        };

        const onChangeLastName = () => {
          dispatch(changeLastName("Scott"));
        };

        return (
          <>
            <div data-testid="value">{value}</div>
            <button data-testid="change-first-name" onClick={onChangeFirstName}>
              Change First Name
            </button>
            <button data-testid="change-last-name" onClick={onChangeLastName}>
              Change Last Name
            </button>
          </>
        );
      });

      render(<TestHarness />, { wrapper });

      // The initial render displays the correct value
      expect(screen.getByTestId("value")).toHaveTextContent("John");
      // The initial render had to run the firstNameState selector
      expect(getFirstName).toHaveBeenCalledTimes(1);
      // The initial render counts as 1 render
      expect(TestHarness).toHaveBeenCalledTimes(1);

      // Change the last name
      userEvent.click(screen.getByTestId("change-last-name"));

      // The first name is still correct
      expect(screen.getByTestId("value")).toHaveTextContent("John");
      // The firstNameState selector was not re-run because tracking
      // determined that the relevant parts of the state didn't change
      expect(getFirstName).toHaveBeenCalledTimes(1);
      // There have been no additional renders because no relevant part(s) of state changed
      expect(TestHarness).toHaveBeenCalledTimes(1);

      // Now change the first name
      userEvent.click(screen.getByTestId("change-first-name"));

      // The first name has been updated
      expect(screen.getByTestId("value")).toHaveTextContent("William");
      // The firstNameState selector was re-run because the relevant part(s) of state changed
      expect(getFirstName).toHaveBeenCalledTimes(2);
      // An additional render occurred because a relevant value changed
      expect(TestHarness).toHaveBeenCalledTimes(2);
    });

    it("does not re-run the selector when re-rendering due to an external cause", () => {
      const getFirstName = jest.fn(
        ({ state }: Accessors<RootState>) => state.person.firstName
      );
      const firstNameState = selector({
        get: getFirstName,
      });

      // Wrapped in jest.fn so that renders can be counted
      const TestHarness: FC = jest.fn(() => {
        const { value } = useTestHarness(firstNameState);
        const [, update] = useState(0);

        const onCauseRerender = () => {
          update((c) => c + 1);
        };

        return (
          <>
            <div data-testid="value">{value}</div>
            <button data-testid="cause-rerender" onClick={onCauseRerender}>
              Cause Rerender
            </button>
          </>
        );
      });

      render(<TestHarness />, { wrapper });

      // The initial render displays the correct firstName
      expect(screen.getByTestId("value")).toHaveTextContent("John");
      // The initial render had to run the firstNameState selector
      expect(getFirstName).toHaveBeenCalledTimes(1);
      // The initial render counts as 1 render
      expect(TestHarness).toHaveBeenCalledTimes(1);

      // Cause an unrelated re-render
      userEvent.click(screen.getByTestId("cause-rerender"));

      // The first name is still correct
      expect(screen.getByTestId("value")).toHaveTextContent("John");
      // The firstNameState selector was not re-run because tracking
      // determined that the relevant parts of the state didn't change
      expect(getFirstName).toHaveBeenCalledTimes(1);
      // There was a re-render due to the external cause
      expect(TestHarness).toHaveBeenCalledTimes(2);
    });

    it("does not re-run the selectorFamily when re-rendering due to an external cause", () => {
      const getProperty = jest.fn(
        (state: RootState, property: keyof PersonState) =>
          state.person[property]
      );
      const getPropertySelector = jest.fn(
        (property: keyof PersonState) =>
          ({ state }: Accessors<RootState>) =>
            getProperty(state, property)
      );
      const personPropertyState = selectorFamily<string, keyof PersonState>({
        get: getPropertySelector,
      });

      // Wrapped in jest.fn so that renders can be counted
      const TestHarness: FC = jest.fn(() => {
        const { value } = useTestHarness(personPropertyState("firstName"));
        const [, update] = useState(0);

        const onCauseRerender = () => {
          update((c) => c + 1);
        };

        return (
          <>
            <div data-testid="value">{value}</div>
            <button data-testid="cause-rerender" onClick={onCauseRerender}>
              Cause Rerender
            </button>
          </>
        );
      });

      render(<TestHarness />, { wrapper });

      // The initial render displays the correct firstName
      expect(screen.getByTestId("value")).toHaveTextContent("John");
      // The initial render had to get the selectorFamily selector
      expect(getPropertySelector).toHaveBeenCalledTimes(1);
      // The initial render had to run the getProperty selector
      expect(getProperty).toHaveBeenCalledTimes(1);
      // The initial render counts as 1 render
      expect(TestHarness).toHaveBeenCalledTimes(1);

      // Cause an unrelated re-render
      userEvent.click(screen.getByTestId("cause-rerender"));

      // The first name is still correct
      expect(screen.getByTestId("value")).toHaveTextContent("John");
      // The getPropertySelector selectorFamily was not re-run because tracking
      // determined that the relevant parts of the state didn't change
      expect(getPropertySelector).toHaveBeenCalledTimes(1);
      // The getProperty selector was not re-run because tracking
      // determined that the relevant parts of the state didn't change
      expect(getProperty).toHaveBeenCalledTimes(1);
      // There was a re-render due to the external cause
      expect(TestHarness).toHaveBeenCalledTimes(2);
    });
  });
});
