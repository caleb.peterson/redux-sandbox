import type { RootState } from "../store";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment } from "../state/counter";
import { createSelector } from "reselect";
import { ChangeEvent, memo, useCallback, useState } from "react";

const counterState = (state: RootState) => {
  console.log("read counter");
  return state.counter.value;
};

const factorState = (state: RootState, factor: number) => {
  return factor;
};

const derivedState = createSelector(
  [counterState, factorState],
  (count, factor) => {
    console.log("read derived");
    return count * factor;
  }
);

const signState = createSelector(counterState, (count) => {
  console.log("read sign");
  return count >= 0 ? "+" : "-";
});

const Sign = memo(() => {
  console.log("render ReselectCounter Sign");
  const sign = useSelector(signState);

  return <div>{sign}</div>;
});

export function ReselectCounter() {
  const [factor, setFactor] = useState(2);
  const onChangeFactor = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    setFactor(parseInt(event.target.value, 10));
  }, []);

  console.group("render ReselectCounter");
  const count = useSelector(counterState);
  // Is this really the state of the art for redux selectors?
  const deriver = useCallback(
    (state: RootState) => derivedState(state, factor),
    [factor]
  );
  const derived = useSelector(deriver);
  const dispatch = useDispatch();
  console.groupEnd();

  return (
    <div>
      <strong>Reselect</strong>
      <div>
        <label htmlFor="factor">Factor</label>
        <input
          name="factor"
          type="number"
          value={factor}
          onChange={onChangeFactor}
        />
      </div>
      <div>
        <button
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>
        <span>{count}</span>
        <button
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>
      </div>
      <div>{derived}</div>
      <Sign />
    </div>
  );
}
