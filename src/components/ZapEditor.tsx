import { useSelector, useDispatch } from "react-redux";

import { update } from "../state/zap";
import { RootState } from "../store";
import { ChangeEvent, useState } from "react";

export const ZapEditor = () => {
  const zap = useSelector((state: RootState) => state.zap.value);
  const dispatch = useDispatch();

  const [value, setValue] = useState(JSON.stringify(zap, undefined, 2));
  const [error, setError] = useState<string | null>(null);

  const onChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
    setError(null);
    setValue(event.target.value);
  };

  const onBlur = () => {
    try {
      const updated = JSON.parse(value);
      dispatch(update(updated));
    } catch (error) {
      if (error instanceof Error) {
        setError(error.message);
      }
    }
  };

  return (
    <div style={{ height: "100%", width: "100%" }}>
      {error}
      <textarea
        style={{
          height: "100%",
          width: "100%",
          boxSizing: "border-box",
          padding: 10,
        }}
        spellCheck={false}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
    </div>
  );
};
