import { RootState } from "../store";
import { memo } from "react";
import { createSelectorToolkit } from "../selector-toolkit";
import { Step, StepParams, StepProperty, Zap } from "../types";
import { useSelector } from "react-redux";

const { selector, selectorFamily, useSelectorValue } =
  createSelectorToolkit<RootState>(useSelector);

const zapState = selector<Zap>({
  get: ({ state }) => state.zap.value,
});

const idState = selector({
  get: ({ get }) => get(zapState).id,
});

const zapTitleState = selector({
  get: ({ get }) => get(zapState).title,
});

const stepsState = selector<Step[]>({
  get: ({ get }) => get(zapState).steps,
});

const stepIdsState = selector({
  get: ({ get }) => get(stepsState).map((step) => step.id),
});

const stepByIdState = selectorFamily<Step | undefined, string>({
  get:
    (stepId) =>
    ({ get }) =>
      get(stepsState).find((step) => step.id === stepId),
});

const stepByIdRequiredState = selectorFamily<Step, string>({
  get:
    (stepId) =>
    ({ get }) => {
      const step = get(stepByIdState(stepId));

      if (!step) {
        throw new Error(`Unable to find step with id ${stepId}`);
      }

      return step;
    },
});

const stepNumberState = selectorFamily<number | undefined, string>({
  get:
    (stepId) =>
    ({ get }) =>
      get(stepsState).findIndex((step) => step.id === stepId) + 1,
});

const stepTitleState = selectorFamily<string, string>({
  get:
    (stepId) =>
    ({ get }) =>
      get(stepByIdRequiredState(stepId)).title,
});

const stepParamsState = selectorFamily<StepParams, string>({
  get:
    (stepId) =>
    ({ get }) =>
      get(stepByIdRequiredState(stepId)).params,
});

type StepPropertyKey = {
  stepId: string;
  property: StepProperty;
};

const stepPropertyState = selectorFamily<string, StepPropertyKey>({
  get:
    ({ stepId, property }) =>
    ({ get }) =>
      get(stepByIdRequiredState(stepId))[property],
});

interface StepProps {
  stepId: string;
}

const StepTitle = memo(({ stepId }: StepProps) => {
  const stepNumber = useSelectorValue(stepNumberState(stepId));
  const stepTitle = useSelectorValue(stepTitleState(stepId));

  return (
    <div>
      {stepNumber}. {stepTitle}
    </div>
  );
});

const StepAppAndEvent = memo(({ stepId }: StepProps) => {
  const app = useSelectorValue(stepPropertyState({ stepId, property: "app" }));
  const action = useSelectorValue(
    stepPropertyState({ stepId, property: "action" })
  );

  return (
    <div>
      <small>
        {app} / {action}
      </small>
    </div>
  );
});

const StepParamsDisplay = memo(({ stepId }: StepProps) => {
  const stepParams = useSelectorValue(stepParamsState(stepId));

  return (
    <pre style={{ margin: "0 0 0 20px" }}>
      {JSON.stringify(stepParams, undefined, 2)}
    </pre>
  );
});

const StepDisplay = memo(({ stepId }: StepProps) => {
  return (
    <div style={{ marginBottom: 20 }}>
      <StepTitle stepId={stepId} />
      <StepAppAndEvent stepId={stepId} />
      <StepParamsDisplay stepId={stepId} />
    </div>
  );
});

export const SelectorKitZap = () => {
  const id = useSelectorValue(idState);
  const title = useSelectorValue(zapTitleState);
  const stepIds = useSelectorValue(stepIdsState);

  return (
    <>
      <div style={{ width: "100%", height: "100%" }}>
        <div>
          <strong>SelectorKitZap</strong>
        </div>
        <strong>{title}</strong>
        <small>{id}</small>
        {stepIds.map((stepId) => (
          <StepDisplay stepId={stepId} key={stepId} />
        ))}
      </div>
    </>
  );
};
