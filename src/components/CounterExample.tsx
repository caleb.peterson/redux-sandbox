import { ReselectCounter } from "./ReselectCounter";
import { SelectorKitCounter } from "./SelectorKitCounter";

export const CounterExample = () => (
  <div
    style={{
      display: "flex",
      flexDirection: "column",
      gap: 20,
      textAlign: "center",
    }}
  >
    <SelectorKitCounter />
    <ReselectCounter />
  </div>
);
