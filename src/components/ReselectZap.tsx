import { useSelector } from "react-redux";
import { RootState } from "../store";
import { createSelector } from "reselect";
import { memo } from "react";
import { StepProperty } from "../types";

const zapState = (state: RootState) => state.zap.value;

const idState = createSelector([zapState], (zap) => zap.id);

const zapTitleState = createSelector([zapState], (zap) => zap.title);

const stepsState = createSelector([zapState], (zap) => zap.steps);

const stepIdsState = createSelector([stepsState], (steps) =>
  steps.map((step) => step.id)
);

const stepIdKey = (_state: RootState, stepId: string) => stepId;

const stepByIdState = createSelector([stepsState, stepIdKey], (steps, stepId) =>
  steps.find((step) => step.id === stepId)
);

const stepNumberState = createSelector(
  [stepsState, stepIdKey],
  (steps, stepId) => steps.findIndex((step) => step.id === stepId) + 1
);

const stepTitleState = createSelector(
  [stepsState, stepIdKey],
  (steps, stepId) => steps.find((step) => step.id === stepId)?.title
);

const stepParamsState = createSelector(
  [stepsState, stepIdKey],
  (steps, stepId) => steps.find((step) => step.id === stepId)?.params
);

type StepPropertyKey = {
  stepId: string;
  property: StepProperty;
};

const stepPropertyKey = (_state: RootState, key: StepPropertyKey) => key;

const stepPropertyState = createSelector(
  [stepsState, stepPropertyKey],
  (steps, { stepId, property }) =>
    steps.find((step) => step.id === stepId)?.[property]
);

interface StepProps {
  stepId: string;
}

const StepTitle = memo(({ stepId }: StepProps) => {
  const stepNumber = useSelector((state: RootState) =>
    stepNumberState(state, stepId)
  );

  const stepTitle = useSelector((state: RootState) =>
    stepTitleState(state, stepId)
  );

  return (
    <div>
      {stepNumber}. {stepTitle}
    </div>
  );
});

const StepAppAndEvent = memo(({ stepId }: StepProps) => {
  const app = useSelector((state: RootState) =>
    stepPropertyState(state, { stepId, property: "app" })
  );
  const action = useSelector((state: RootState) =>
    stepPropertyState(state, { stepId, property: "action" })
  );

  return (
    <div>
      <small>
        {app} / {action}
      </small>
    </div>
  );
});

const StepParams = memo(({ stepId }: StepProps) => {
  const stepParams = useSelector((state: RootState) =>
    stepParamsState(state, stepId)
  );

  return (
    <pre style={{ margin: "0 0 0 20px" }}>
      {JSON.stringify(stepParams, undefined, 2)}
    </pre>
  );
});

const StepDisplay = memo(({ stepId }: StepProps) => {
  return (
    <div style={{ marginBottom: 20 }}>
      <StepTitle stepId={stepId} />
      <StepAppAndEvent stepId={stepId} />
      <StepParams stepId={stepId} />
    </div>
  );
});

export const ReselectZap = () => {
  const id = useSelector(idState);
  const title = useSelector(zapTitleState);
  const stepIds = useSelector(stepIdsState);

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <div>
        <strong>ReselectZap</strong>
      </div>
      <strong>{title}</strong>
      <small>{id}</small>
      {stepIds.map((stepId) => (
        <StepDisplay stepId={stepId} key={stepId} />
      ))}
    </div>
  );
};
