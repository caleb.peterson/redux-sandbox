import type { RootState } from "../store";
import { useDispatch, useSelector } from "react-redux";
import { decrement, increment } from "../state/counter";
import { createSelectorToolkit } from "../selector-toolkit";
import { ChangeEvent, memo, useCallback, useState } from "react";

const { selector, selectorFamily, useSelectorValue } =
  createSelectorToolkit<RootState>(useSelector);

const counterState = selector<number>({
  get: ({ state }) => {
    return state.counter.value;
  },
});

const derivedState = selectorFamily<number, number>({
  get:
    (factor) =>
    ({ get }) => {
      return get(counterState) * factor;
    },
});

const signState = selector<string>({
  get: ({ get }) => {
    return get(counterState) >= 0 ? "+" : "-";
  },
});

const Sign = memo(() => {
  console.log("render SelectorKitCounter Sign");
  const sign = useSelectorValue(signState);

  return <div>{sign}</div>;
});

export function SelectorKitCounter() {
  const [factor, setFactor] = useState(2);
  const onChangeFactor = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    setFactor(parseInt(event.target.value, 10));
  }, []);

  console.group("render SelectorKitCounter");
  const count = useSelectorValue(counterState);
  const doubled = useSelectorValue(derivedState(factor));
  const dispatch = useDispatch();
  console.groupEnd();

  return (
    <div>
      <strong>SelectorKit</strong>
      <div>
        <label htmlFor="factor">Factor</label>
        <input
          name="factor"
          type="number"
          value={factor}
          onChange={onChangeFactor}
        />
      </div>
      <div>
        <button
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>
        <span>{count}</span>
        <button
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>
      </div>
      <div>{doubled}</div>
      <Sign />
    </div>
  );
}
